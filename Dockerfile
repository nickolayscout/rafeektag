# Pull nginx base image
FROM nginx:latest

# Expost port 8080
EXPOSE 8080

# Copy custom configuration file from the current directory
COPY nginx.conf /etc/nginx/nginx.conf

# Copy static assets
COPY ./src/index.html /usr/share/nginx/html

# Start up nginx server
CMD ["nginx"]